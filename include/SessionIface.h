/*
** Copyright (c) 2020 Domnin V.N.
** All rights reserved
**
**
*/

#ifndef TEMPLATE_SESSION_IFACE_H
#define TEMPLATE_SESSION_IFACE_H

#include <type_traits>
#include <string>
#include <functional>
#include <boost/asio.hpp>

#include <Package.h>

class SessionIface
{
public:
    static SessionIface* CreateSession(boost::asio::io_service *ioService,
                         const std::shared_ptr<boost::asio::ip::tcp::socket> &socket,
                         std::size_t bufferDataSize);

    virtual void doRead() = 0;
    virtual std::string ip() = 0;
    virtual bool sendPackage(const Package &package) = 0;
    virtual bool sendData(std::vector<char> data) = 0;
    virtual boost::asio::ip::tcp::socket & getSocket() = 0;

    virtual void bindReadErrorRecived(std::function<void (std::string)> handler) = 0;
    virtual void bindWriteErrorRecived(std::function<void (std::string)> handler) = 0;
    virtual void bindPackageRecived(std::function<void (const Package &)> handler) = 0;

    virtual ~SessionIface() {}
};

#endif // TEMPLATE_SESSION_IFACE_H
