#ifndef PACKAGE_H
#define PACKAGE_H

#include <string>
#include <vector>

class PackagePrivate;
class Session;

class Package
{
    friend Session;
public:
    Package(std::string message);
    Package(char *buf_, ulong sizeBuf);
    Package(std::vector<char> message);
    std::vector<char> data() const;
    ~Package();

private:
   Package(PackagePrivate &packagePrivate);
   PackagePrivate * const m_pImpl;
};

#endif // PACKAGE_H
